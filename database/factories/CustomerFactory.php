<?php

use app\Customer;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Customer::class, function (Faker $faker) {
	$date = $faker->dateTimeBetween('-1 days','+1 months')->format('Y-m-d H:i:s');
    return [
    	'client' => $faker->randomElement(['Acme','Apple','Microsoft']),
        'product' => $faker->text,
        'total' => $faker->randomDigit,
        'created_at' => $date,
        'updated_at' => $date,
    ];
});
