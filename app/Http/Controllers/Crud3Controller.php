<?php
namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Khill\Lavacharts\Lavacharts;
use Mail;


class Crud3Controller extends Controller
{
    public function index(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('client', $request->has('client') ? $request->get('client') : ($request->session()->has('client') ? $request->session()->get('client') : -1));

        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'product'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'asc'));
        $customers = new Customer();
        if ($request->session()->get('client') != -1)
            $customers = $customers->where('client', $request->session()->get('client'));

        $customers = $customers->where('product', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);


            $lava = new Lavacharts;

            $temperatures = $lava->DataTable();

            $temperatures->addColumns([
                ['datetime', 'created_at'],
                ['number', 'total'],
            ]);

            $customer =  Customer::all();
            foreach ($customer as $key => $value) {
                $rowData = [
                  date('Y-m-d', strtotime($value->created_at)), $value->total
                ];

                $temperatures->addRow($rowData); 
            }

            $lava->LineChart('Temps', $temperatures, [
                'title' => 'Search results'
            ]);


        if ($request->ajax())
            return view('crud_3.index', compact('customers','lava'));
        else
            return view('crud_3.ajax', compact('customers','lava'));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get'))
            return view('crud_3.form');
        else {
            $rules = [
                'client' => 'required',
                'product' => 'required',
                'total' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $customer = new Customer();
            $customer->product = $request->product;
            $customer->client = $request->client;
            $customer->total = $request->total;
            $customer->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('laravel-crud-search-sort-ajax')
            ]);
        }
    }

    public function delete($id)
    {
        Customer::destroy($id);
        return redirect('/laravel-crud-search-sort-ajax');
    }

    public function update(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('crud_3.form', ['customer' => Customer::find($id)]);
        else {
            $rules = [
                'product' => 'required',
                'total' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $customer = Customer::find($id);
            $customer->product = $request->product;
            $customer->client = $request->client;
            $customer->total = $request->total;
            $customer->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('laravel-crud-search-sort-ajax')
            ]);
        }
    }
    /* Test mail function */ 
    public function sendmail(Request $request)
    {
        $to_name = 'Andrew';
        $to_email = 'quantumwise555@gmail.com';
        $customer = Customer::all();
        $data = [
            'informations' => $customer,
        ];
            
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Artisans Web Testing Mail');
            $message->from('quantumwise555@gmail.com','Artisans Web');
        });
        return redirect('/laravel-crud-search-sort-ajax');
    }

}