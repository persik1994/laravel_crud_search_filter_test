<table border="1" style="text-align: center;">
	<thead>
		<th>No</th>
		<th>Client</th>
		<th>Product</th>
		<th>Total</th>
		<th>Date</th>
	</thead>
	<tbody>
		@foreach($informations as $information)
		<tr>
			<td>{{ $information->id }}</td>
			<td>{{ $information->client }}</td>
			<td>{{ $information->product }}</td>
			<td>{{ $information->total }}</td>
			<td>{{ $information->created_at }}</td>
		</tr>
		@endforeach
	</tbody>
</table>