<div class="container">
    <div class="float-right">
        <a href="javascript:ajaxLoad('{{url('laravel-crud-search-sort-ajax/create')}}')"
           class="btn btn-primary">New</a>
    </div>
    <h1 style="font-size: 2.2rem">Test Task</h1>
    <hr/>

    <div id="chart-div"></div>
    {!! $lava->render('LineChart', 'Temps', 'chart-div') !!}

    <hr/>
    <div class="row">
       <div class="container" id="app">
           <coin-add-component></coin-add-component>
           <chart-component></chart-component>
       </div>
        <div class="col-sm-4 form-group">
            {!! Form::select('client',['-1'=>'Select client','Acme'=>'Acme','Apple'=>'Apple','Microsoft'=>'Microsoft'],request()->session()->get('client'),['class'=>'form-control','onChange'=>'ajaxLoad("'.url("laravel-crud-search-sort-ajax").'?client="+this.value)']) !!}
        </div>
        <div class="col-sm-5 form-group">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('laravel-crud-search-sort-ajax')}}?search='+this.value)"
                       placeholder="Search product" name="search"
                       type="text" id="search"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-warning"
                            onclick="ajaxLoad('{{url('laravel-crud-search-sort-ajax')}}?search='+$('#search').val())"
                    >
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle" width="10%">
                <a href="javascript:ajaxLoad('{{url('laravel-crud-search-sort-ajax?field=client&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Client
                </a>
                {{request()->session()->get('field')=='client'?(request()->session()->get('sort')=='asc'?'▴':'▾'):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('laravel-crud-search-sort-ajax?field=product&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Product
                </a>
                {{request()->session()->get('field')=='product'?(request()->session()->get('sort')=='asc'?'▴':'▾'):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('laravel-crud-search-sort-ajax?field=total&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Total
                </a>
                {{request()->session()->get('field')=='total'?(request()->session()->get('sort')=='asc'?'▴':'▾'):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('laravel-crud-search-sort-ajax?field=created_at&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Date
                </a>
                {{request()->session()->get('field')=='created_at'?(request()->session()->get('sort')=='asc'?'▴':'▾'):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Actions</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($customers as $customer)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $customer->client }}</td>
                <td style="vertical-align: middle">{{ $customer->product }}</td>
                <td style="vertical-align: middle">{{ $customer->total }}</td>
                <td style="vertical-align: middle">{{ $customer->created_at }}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit"
                       href="javascript:ajaxLoad('{{url('laravel-crud-search-sort-ajax/update/'.$customer->id)}}')">
                        Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete"
                       href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('laravel-crud-search-sort-ajax/delete/'.$customer->id)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$customers->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>


</div>