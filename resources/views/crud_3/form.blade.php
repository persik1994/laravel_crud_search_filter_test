<div class="container">
    <div class="col-md-8 offset-md-2">
        <h1>{{isset($customer)?'Edit':'New'}} Customer</h1>
        <hr/>
        @if(isset($customer))
            {!! Form::model($customer,['method'=>'put','id'=>'frm']) !!}
        @else
            {!! Form::open(['id'=>'frm']) !!}
        @endif
        <!-- Client name -->
        <div class="form-group row required">
            {!! Form::label("client","Client",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
            <div class="col-md-8">
                {!! Form::select("client",['Acme'=>'Acme','Apple'=>'Apple','Microsoft'=>'Microsoft'],null,["class"=>"form-control"]) !!}
            </div>
        </div>
        <!-- Product name -->
        <div class="form-group row required">
            {!! Form::label("product","Product",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
            <div class="col-md-8">
                {!! Form::text("product",null,["class"=>"form-control".($errors->has('product')?" is-invalid":""),"autofocus",'placeholder'=>'Product']) !!}
                <span id="error-product" class="invalid-feedback"></span>
            </div>
        </div>
        <!-- Total quantity -->
        <div class="form-group row required">
            {!! Form::label("total","Total",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
            <div class="col-md-8">
                {!! Form::text("total",null,["class"=>"form-control".($errors->has('total')?" is-invalid":""),"autofocus",'placeholder'=>'Total']) !!}
                <span id="error-total" class="invalid-feedback"></span>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-3 col-lg-2"></div>
            <div class="col-md-4">
                <a href="javascript:ajaxLoad('{{url('laravel-crud-search-sort-ajax')}}')" class="btn btn-danger">
                    Back</a>
                {!! Form::button("Save",["type" => "submit","class"=>"btn
            btn-primary"])!!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>